
export default function Home() {
  return (
    <main className='w-full h-screen bg-green-500 flex flex-col items-center justify-center font-monts'>
      <div className="font-bold text-white text-xl">Welcome!</div>
      <div className=" text-white text-lg">EMOS Frontend Technical Test</div>
    </main>
  )
}
