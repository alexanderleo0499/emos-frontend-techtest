import { NextRequest } from "next/server";

interface Song{
    id:string
    title:string
    artist:string
}

type Playlist = Song[]

export async function GET(request:NextRequest){
    const playlist:Playlist = [
        {
            id:"S001",
            artist:"Joseph Vincent",
            title:"Only You"
        },
        {
            id:"S002",
            title:"Restless Love",
            artist:"Sezairi"
        },
        {
            id:"S003",
            title:"Eternal Flame",
            artist:"Shane Filan"
        },
        {
            id:"S004",
            title:"As It Was",
            artist:"PREP"
        },
        {
            id:"S005",
            title:"Just The Way You Are",
            artist:"Bruno Mars"
        },
        {
            id:"S006",
            title:"Whiskey Bottle",
            artist:"GANGGA"
        },
        {
            id:"S007",
            title:"New Light",
            artist:"John Mayer"
        },
        {
            id:"S008",
            title:"Blue Jeans",
            artist:"GANGGA"
        },
        {
            id:"S009",
            title:"happy",
            artist:"Skinnyfabs"
        },
        {
            id:"S010",
            title:"Marry You",
            artist:"Bruno Mars"
        }
    ]
    
    return Response.json({
        data:playlist,
        response:{
            status:200,
            errMsg:""
        }
    },{status:200})
}